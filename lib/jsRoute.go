package jsroute

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"sync"
)

func uint16Hex(hex string) (ret uint16, err error) {
	hexLen := len(hex)
	if hexLen > 4 {
		err = errors.New("error")
		return
	}
	i := 0
	for i < hexLen && hex[i] == '0' { // remove '0' left padding
		i++
	}
	for i < hexLen {
		chr := hex[i]
		if chr > 47 && chr < 58 {
			ret = (ret << 4) | uint16(chr-48)
		} else if chr > 64 && chr < 71 {
			ret = (ret << 4) | uint16(chr-55)
		} else {
			err = errors.New("error")
			return
		}
		i++
	}
	return
}
func hexUint16(d uint16) string {
	hex := []byte("0000")
	i := 3
	for d != 0 {
		kd := d & 15
		if kd < 10 {
			hex[i] = byte(kd + 48)
		} else if kd < 16 {
			hex[i] = byte(kd + 55)
		}
		d = d >> 4
		i--
	}
	return string(hex)
}

type (
	HF      func(http.ResponseWriter, *http.Request)
	handler struct {
		paramsNum  int
		isWildcard bool
		fn         HF
	}
	Node struct {
		Idx  string
		Next tree
	}
	tree map[string]*Node
	Mux  struct {
		t          tree
		hndlrs     []handler
		paramsPool sync.Pool
		paramsNum  int
	}
	PW struct {
		http.ResponseWriter
		Params   []string
		Wildcard string
	}
)

func New() (m *Mux) {
	m = &Mux{
		tree{},
		[]handler{},
		sync.Pool{
			New: func() (ret interface{}) {
				ret = make([]string, m.paramsNum)
				return
			},
		},
		0,
	}
	return
}
func (m *Mux) Handle(path string, fn HF) {
	t := m.t
	prmsNum := 0
	var isWldCrd bool
	sgmnts := strings.Split(path, "/")
	for _, sgmnt := range sgmnts {
		if sgmnt == "#" {
			prmsNum++
			if m.paramsNum < prmsNum {
				m.paramsNum = prmsNum
			}
		} else if sgmnt == "*" {
			isWldCrd = true
		}
	}
	last := len(sgmnts) - 1
	for i, sgmnt := range sgmnts {
		nd, ok := t[sgmnt]
		if !ok {
			nd = &Node{}
			nd.Idx = "FFFF"
			nd.Next = tree{}
			t[sgmnt] = nd
		}
		if i == last {
			nd.Idx = hexUint16(uint16(len(m.hndlrs)))
			m.hndlrs = append(m.hndlrs, handler{prmsNum + 1, isWldCrd, fn})
			return
		}
		t = nd.Next
	}
}
func (m *Mux) JS() []byte {
	bs, _ := json.Marshal(m.t)
	return []byte(fmt.Sprintf(`
function action(methodNotAllowed, notFound){
	return function( path ){
		var route = %s
		var sgmnts = path.split('/'), nd = {}, len = 0, params = [], wildcard = '';
		for(var i=0; i<sgmnts.length; i++){
    		nd = route[sgmnts[i]]
    		if(nd)route = nd.Next; else {
    			if(i==0){
					methodNotAllowed()
					return ''
				}  
    	    	nd = route['#']
    	    	if(nd){
    	    		route = nd.Next
    	    		params.push(sgmnts[i])
    	    	} else {
    	    		nd = route['*']
    	    		if(nd){
    	      			wildcard = path.substr(len)
    	      			break
    	    		} else {
						notFound()
						return ''
					} 
    	    	} 
    		}
    		len = len + sgmnts[i].length + 1
    	}
    	return nd.Idx + (params ? '/' + params.join('/') : '') + (wildcard ? '/' + wildcard : '')
	}
}

	`, bs))
}
func (m *Mux) CB(w http.ResponseWriter, r *http.Request) {
	rURL := r.URL
	pathLen := len(rURL.Path)
	if pathLen == 0 || rURL.Path[0] != '/' {
		println("invalid url")
		return
	}
	for rURL.Path[pathLen-1] == '/' {
		pathLen--
	}
	var (
		hndlrsIdx uint16
		err       error
		prms      []string
		prmsIdx   = 0
	)
	i := 1
	for j := 2; j <= pathLen; j++ {
		if j == pathLen || rURL.Path[j] == '/' {
			if i == 1 {
				hndlrsIdx, err = uint16Hex(rURL.Path[i:j])
				if err != nil || hndlrsIdx >= uint16(len(m.hndlrs)) {
					return
				}
			} else if (prmsIdx + 1) < m.hndlrs[hndlrsIdx].paramsNum {
				if prmsIdx == 0 {
					prms = m.paramsPool.Get().([]string)
				}
				prms[prmsIdx] = rURL.Path[i:j]
				prmsIdx++
			} else if m.hndlrs[hndlrsIdx].isWildcard {
				if prmsIdx != 0 {
					m.hndlrs[hndlrsIdx].fn(PW{w, prms, rURL.Path[i:]}, r)
					m.paramsPool.Put(prms)
					return
				}
				m.hndlrs[hndlrsIdx].fn(PW{w, nil, rURL.Path[i:]}, r)
				return
			} else if j == pathLen {
				if prmsIdx != 0 {
					m.hndlrs[hndlrsIdx].fn(PW{w, prms, ""}, r)
					m.paramsPool.Put(prms)
					return
				}
				m.hndlrs[hndlrsIdx].fn(w, r)
				return
			}
			j++
			i = j
		}
	}
}
