package main

import (
	"fmt"
	"net/http"

	"gitlab.com/birowo/gojsroute/lib"
)

func hndlr(w http.ResponseWriter, r *http.Request) {
	pw, ok := w.(jsroute.PW)
	if ok {
		fmt.Fprintln(w, r.Method, r.URL.Path, pw.Params, pw.Wildcard)
		return
	}
	fmt.Fprintln(w, r.Method, r.URL.Path)
}
func html(js []byte, cb jsroute.HF) (ret http.HandlerFunc) {
	ret = func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "text/html; charset=utf-8")
		if r.URL.Path == "/" {
			fmt.Fprintf(w, `
<form method="GET" action="/abc/123/def/456/ghi/789/012" target="iframe1" onsubmit="return sbmt(this)">
	<input type="submit" value="GET/abc/123/def/456/ghi/789/012">
</form>
<div id="rspns"></div>
<iframe name="iframe1" onload="response(this)" style="display:none"></iframe>
<script>
	%s
	var response = (function(){
		var s = ''
		return function (me){
			s += (me.contentDocument||me.contentWindow.document).body.innerHTML + '<br>'
			rspns.innerHTML = s
		}
	})()
	var fAction = action(function(){ alert('method not allowed') }, function(){ alert('resource not found') })
	function sbmt(me){
		var base = location.protocol+'//'+location.host
		vAction = fAction(me.method.toUpperCase() + me.action.replace(base, ''))
		if( vAction ){
			me.action = vAction
			return true
		} 
		return false
	}
</script>
			`, js)
			return
		}
		cb(w, r)
	}
	return
}
func main() {
	m := jsroute.New()
	m.Handle("GET/abc/#/def/#/ghi/*", hndlr)
	m.Handle("GET/abc/ghi/jkl", hndlr)
	m.Handle("GET/abc/ghi", hndlr)
	m.Handle("GET/abc/ghi/jkl/mno", hndlr)

	http.ListenAndServe(":8080", html(m.JS(), m.CB))
}
